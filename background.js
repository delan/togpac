const browserAction = browser.browserAction;
const proxySettings = browser.proxy.settings;

proxySettings.get({}).then(render);
proxySettings.onChange.addListener(render);

browserAction.onClicked.addListener(async (_, { modifiers }) => {
  const { levelOfControl } = await proxySettings.get({});

  switch (levelOfControl) {
    case "controllable_by_this_extension":
      await proxySettings.set({ value: { proxyType: "none" } });
      break;
    case "controlled_by_this_extension":
      await proxySettings.clear({});
      break;
  }
});

async function render({ levelOfControl }) {
  browserAction.setBadgeBackgroundColor({ color: "black" });
  browserAction.setBadgeTextColor({ color: "white" });

  if (!(await browser.extension.isAllowedIncognitoAccess())) {
    browserAction.setTitle({
      title:
      "Warning: can’t control proxy settings! Please enable this extension in private browsing: go to about:addons, click Quick Proxy Toggle, click Allow.",
    });
    browserAction.setBadgeBackgroundColor({ color: "red" });
    browserAction.setBadgeTextColor({ color: "white" });
    browserAction.setBadgeText({ text: "⚠" });
    return;
  }

  switch (levelOfControl) {
    case "not_controllable":
      browserAction.setTitle({ title: "Warning: can’t control proxy settings (unknown reason)!" });
      browserAction.setBadgeBackgroundColor({ color: "red" });
      browserAction.setBadgeTextColor({ color: "white" });
      browserAction.setBadgeText({ text: "⚠" });
      return;
    case "controlled_by_other_extensions":
      browserAction.setTitle({
        title: "Warning: can’t control proxy settings (another extension is controlling them)!",
      });
      browserAction.setBadgeBackgroundColor({ color: "red" });
      browserAction.setBadgeTextColor({ color: "white" });
      browserAction.setBadgeText({ text: "⚠" });
      return;
    case "controllable_by_this_extension":
      browserAction.setTitle({ title: "Using your normal proxy settings. Click to use no proxy." });
      browserAction.setBadgeBackgroundColor({ color: "cyan" });
      browserAction.setBadgeTextColor({ color: "black" });
      browserAction.setIcon({ path: "proxy.svg" });
      browserAction.setBadgeText({ text: "on" });
      return;
    case "controlled_by_this_extension":
      browserAction.setTitle({ title: "Using no proxy. Click to use your normal proxy settings." });
      browserAction.setBadgeBackgroundColor({ color: "black" });
      browserAction.setBadgeTextColor({ color: "white" });
      browserAction.setIcon({ path: "no-proxy.svg" });
      browserAction.setBadgeText({ text: "no" });
      return;
  }
}
